pragma solidity >=0.5.22 <0.7.0;

contract PayAsYouGoContainer {

    address public endUserAddress;
    uint public containerStartTime = 0;

    uint public remainingMinutes = 100;
    string public streamUrl;
    string public imageName;

    uint public minFps = 10;

    event Start(address indexed endUserAddress, uint startTime);
    event Stop(address indexed endUserAddress, uint stopTime);

    constructor(string memory _streamUrl, string memory _imageName, uint _minFps, address _endUserAddress) public payable {

        endUserAddress = _endUserAddress;
        streamUrl = _streamUrl;
        imageName = _imageName;

        minFps = _minFps;
    }

    function recalculateTime() public {
        remainingMinutes -= (now - containerStartTime) / 60;
        if (remainingMinutes <= 0) {
            stopContainer();
        }
    }

    function topUp() public payable {
        remainingMinutes += msg.value;
    }

    function startContainer() public {
        require(remainingMinutes >= 60);
        require(containerStartTime == 0);
        containerStartTime = now;

        emit Start(endUserAddress, containerStartTime);
    }

    function stopContainer() public {
        require(containerStartTime > 0);
        remainingMinutes -= (now - containerStartTime) / 60;
        containerStartTime = 0;

        emit Stop(endUserAddress, now);
    }
}
