pragma solidity ^0.4.25;

contract FixedPrice {

    address public owner;
    address public endUserAddress;
    uint public price;
    uint public durationSeconds;
    uint amount = 0;

    constructor(uint _price, uint _durationSeconds, address _endUserAddress) public {
        require(_durationSeconds > 0 && _price > 0);

        owner = msg.sender;
        price = _price;
        durationSeconds = _durationSeconds;
        endUserAddress = _endUserAddress;
    }

    function payVCService() public payable {
        require(endUserAddress == msg.sender);
        require(msg.value >= price);

        msg.sender.send(msg.value);
    }
}