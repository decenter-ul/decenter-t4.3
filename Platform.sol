// SPDX-License-Identifier: UNLICENCED
pragma solidity ^0.6.12;
pragma experimental ABIEncoderV2;

import "https://raw.githubusercontent.com/smartcontractkit/chainlink/develop/contracts/src/v0.6/ChainlinkClient.sol";
import "https://bitbucket.org/aleksvujic/smartcontracts/raw/7c6b308bcc011d3d2738f623d61070c9c7140ee3/SafeMath.sol";
import "https://bitbucket.org/aleksvujic/smartcontracts/raw/7c6b308bcc011d3d2738f623d61070c9c7140ee3/Structs.sol";
import "https://bitbucket.org/aleksvujic/smartcontracts/raw/7c6b308bcc011d3d2738f623d61070c9c7140ee3/Helpers.sol";

contract SafeMath {
    function safeMul(uint a, uint b) internal pure returns(uint) {
        uint c = a * b;
        require(a == 0 || c / a == b);
        return c;
    }

    function safeSub(uint a, uint b) internal pure returns(uint) {
        require(b <= a);
        return a - b;
    }

    function safeAdd(uint a, uint b) internal pure returns(uint) {
        uint c = a + b;
        require(c >= a && c >= b);
        return c;
    }
}

contract Helpers {
	function bytesToAddress(bytes memory bys) public pure returns(address addr) {
        assembly {
            addr := mload(add(bys, 20))
        }
    }

    function sliceUint(bytes memory bs, uint start_) public pure returns(uint) {
        uint x;
        assembly {
            x := mload(add(bs, add(0x20, start_)))
        }
        return x;
    }

    function strConcat(string memory _a, string memory _b, string memory _c, string memory _d, string memory _e) internal pure returns (string memory) {
        bytes memory _ba = bytes(_a);
        bytes memory _bb = bytes(_b);
        bytes memory _bc = bytes(_c);
        bytes memory _bd = bytes(_d);
        bytes memory _be = bytes(_e);
        string memory abcde = new string(_ba.length + _bb.length + _bc.length + _bd.length + _be.length);
        bytes memory babcde = bytes(abcde);
        uint k = 0;
        for (uint i = 0; i < _ba.length; i++) babcde[k++] = _ba[i];
        for (uint i = 0; i < _bb.length; i++) babcde[k++] = _bb[i];
        for (uint i = 0; i < _bc.length; i++) babcde[k++] = _bc[i];
        for (uint i = 0; i < _bd.length; i++) babcde[k++] = _bd[i];
        for (uint i = 0; i < _be.length; i++) babcde[k++] = _be[i];
        return string(babcde);
    }

    function toString(bytes memory data) public pure returns(string memory) {
        bytes memory alphabet = "0123456789abcdef";

        bytes memory str = new bytes(2 + data.length * 2);
        str[0] = "0";
        str[1] = "x";
        for (uint i = 0; i < data.length; i++) {
            str[2 + i * 2] = alphabet[uint(uint8(data[i] >> 4))];
            str[3 + i * 2] = alphabet[uint(uint8(data[i] & 0x0f))];
        }
        return string(str);
    }

    function uint2str(uint _i) public pure returns(string memory _uintAsString) {
        if (_i == 0) {
            return "0";
        }
        uint j = _i;
        uint len;
        while (j != 0) {
            len++;
            j /= 10;
        }
        bytes memory bstr = new bytes(len);
        uint k = len - 1;
        while (_i != 0) {
            bstr[k--] = byte(uint8(48 + _i % 10));
            _i /= 10;
        }
        return string(bstr);
    }
    
    function addressToString(address _address) public pure returns(string memory) {
        bytes32 _bytes = bytes32(uint256(_address));
        bytes memory HEX = "0123456789abcdef";
        bytes memory _string = new bytes(42);
        _string[0] = '0';
        _string[1] = 'x';
        for(uint i = 0; i < 20; i++) {
            _string[2+i*2] = HEX[uint8(_bytes[i + 12] >> 4)];
            _string[3+i*2] = HEX[uint8(_bytes[i + 12] & 0x0f)];
        }
        return string(_string);
    }
}

contract Platform is SafeMath, Helpers, ChainlinkClient {
    using Chainlink for Chainlink.Request;
	
	    struct CProvider {
        string name;
        bool Allowed;

        uint32 maxRam;
        uint24 maxCpus;
        uint24 maxGpus;
        uint24 maxRunning;

        uint32 usedRam;
        uint24 usedCpus;
        uint24 usedGpus;
        uint24 usedInstances;

        uint32 id;
        address addr;
    }

    struct AiMethod {
        address creator;
        string name;
        uint32 ram;
        uint24 cpu;
        bool gpuRequired;
        uint price;
        bool Allowed;
        bool Active;
        string ipfsCID;
        bool onlyAllowedUsers;
        uint32 id;
        bool deleted;
    }

    struct LockedFunds {
        uint ethProvider;
        address provider;
        uint ethAiCreator;
        address aiCreator;
        uint ethFee;
        uint32 releaseBlock;
        address buyer;
        uint32 timeInSec;
        uint32 methodId;
        bool complaintWritten;
    }
    
    // STATE VARIABLES
    uint public fulfillCounter = 0;
    uint32 public methodCounter = 0;
    uint32 public dealIdCounter = 0;
    uint32 public hostCounter = 1;
    uint32 public waitNumberOfBlocksBeforeStart = 5;
    uint32 public waitNumberOfBlocksAfterStart = 5;
    address public admin;
    address public feeAccount;
    uint public feeMake; // pay -> AI method creator
    uint public feeTake; // % to platform.
    uint24 public minTime = 10; // minimum rent time of AI method
    uint24 public maxTime = 10000000;
    // oracle
    string public url0;
    string public url1;
    string public url2;
    address public oracleAddr;
    bytes32 public jobId;
    uint256 public oracleFee;

    // MAPPINGS
    mapping(address => CProvider) public hosts;
    mapping(uint32 => address) public hostsIndex; // easy way to find providers (allowed providers)
    mapping(uint32 => AiMethod) public aiMethods;
    mapping(uint32 => mapping(address => uint)) providersPrice;
    mapping(address => uint) public eth;
    mapping(uint32 => LockedFunds) public locked;
    mapping(bytes32 => BuyAiData) public buyAiData;

    // EVENTS
    event AssignedDealId(uint32 dealId);
    event BuyAi(address userBuying, uint32 aiMethodId, address provider, uint24 timeInSec, uint32 dealId, string videoStreamUrl);
    event UserAllowedOracle(uint32 dealId, bool allowed);

    constructor() public {
        admin = msg.sender;
        feeAccount = msg.sender;
        feeMake = 1200000;
        feeTake = 18; // = 1,8%

        // oracle
        setPublicChainlinkToken();
        url0 = "http://194.249.2.111:30091/api/v1/user_method_permission/isUserAllowedToUseMethodInt?userAddress=";
        url1 = "&aiMethodId=";
        url2 = strConcat("&contractAddress=", addressToString(address(this)), "", "", "");
        oracleAddr = 0xc57B33452b4F7BB189bB5AfaE9cc4aBa1f7a4FD8;
        jobId = "d5270d1c311941d0b08bead21fea7747";
        oracleFee = 0.1 * 10 ** 18;
    }

    modifier onlyAdmin() {
        require(msg.sender == admin);
        _;
    }
    
    function changeOracleData(string memory url0_, string memory url1_, address oracleAddr_, uint256 oracleFee_, bytes32 jobId_) public onlyAdmin {
        url0 = url0_;
        url1 = url1_;
        oracleAddr = oracleAddr_;
        oracleFee = oracleFee_;
        jobId = jobId_;
    }

    function changeFeeAccount(address feeAccount_) public onlyAdmin {
        feeAccount = feeAccount_;
    }

    function changeWaitTime(uint32 waitNumberOfBlocksBeforeStart_, uint32 waitNumberOfBlocksAfterStart_) public onlyAdmin {
        waitNumberOfBlocksBeforeStart = waitNumberOfBlocksBeforeStart_;
        waitNumberOfBlocksAfterStart = waitNumberOfBlocksAfterStart_;
    }

    function changeFeeMake(uint feeMake_) public onlyAdmin {
        feeMake = feeMake_;
    }

    function changeFeeTake(uint16 feeTake_) public onlyAdmin {
        require(feeTake_ < 500); // less than 50%, 0.5 = (/ 1000) * 500
        feeTake = feeTake_;
    }

    function allowCProvider(address provider_, bool allow_) public onlyAdmin {
        CProvider storage host = hosts[provider_];
        require(host.addr != address(0), "e1"); // provider does not exist
        host.Allowed = allow_;
    }

    function allowAiMethod(uint32 ai_method_id_, bool allow_) public onlyAdmin {
        AiMethod storage method = aiMethods[ai_method_id_];
        require(method.ram != 0, "e1"); // method doesn't exist
        require(!method.deleted, "e2"); // method is deleted
        method.Allowed = allow_;
    }

    // Start after confirmation time (10 blocks). ???????????????????????????
    function start(uint32 dealId_) public onlyAdmin {
        // MQTT link encrypted with buyer address.
        LockedFunds storage funds = locked[dealId_];
        require(funds.releaseBlock != 0, "e1"); // deal doesn't exist
        require(funds.ethFee + funds.ethProvider + funds.ethAiCreator != 0, "e2"); // no locked funds found (if user took eth)
        funds.releaseBlock = uint32(block.number + (funds.timeInSec / 13) + waitNumberOfBlocksAfterStart);
    }

    function delivered(uint32 dealId_, bool error_) public onlyAdmin {
        LockedFunds memory funds = locked[dealId_];
        require(funds.releaseBlock != 0, "e1"); // this dealId doesn't exist or no funds
        if (error_) {
            returnToBuyer(dealId_);
        } else {
            releaseFunds(dealId_);
        }
        //release CPU, RAM, GPU and instance.
        CProvider storage provider = hosts[funds.provider];
        AiMethod memory method = aiMethods[funds.methodId];
        provider.usedRam = uint32(safeSub(provider.usedRam, method.ram));
        provider.usedCpus = uint24(safeSub(provider.usedCpus, method.cpu));
        provider.usedInstances = uint24(safeSub(provider.usedInstances, 1));
        if (method.gpuRequired) {
            provider.usedGpus = uint24(safeSub(provider.usedGpus, 1));
        }
    }

    //----------------------------- Provider
    function newCProvider(string memory name_, uint32 maxRam_, uint24 maxCpus_, uint24 maxGpus_, uint24 maxRunning_) public {
        require(hosts[msg.sender].addr == address(0), "e1"); // provider already exists
        hosts[msg.sender] = CProvider(name_, false, maxRam_, maxCpus_, maxGpus_, maxRunning_, 0, 0, 0, 0, 0, msg.sender);
        hostCounter += 1;
        hostsIndex[hostCounter] = msg.sender;
        hosts[msg.sender].id = hostCounter;
    }

    function changeCProviderData(uint32 maxRam_, uint24 maxCpus_, uint24 maxGpus_, uint24 maxRunning_) public {
        CProvider storage host = hosts[msg.sender];
        require(host.id != 0, "e1"); // provider does not exist
        host.maxRam = maxRam_;
        host.maxCpus = maxCpus_;
        host.maxGpus = maxGpus_;
        host.maxRunning = maxRunning_;
    }

    function setContainerCost(uint32 ai_method_id_, uint containerCost_) public {
        CProvider memory host = hosts[msg.sender];
        AiMethod memory method = aiMethods[ai_method_id_];
        require(host.Allowed && method.ram != 0, "e1"); // provider not allowed or method does not exist
        if (containerCost_ == 0) {
            delete providersPrice[ai_method_id_][msg.sender];
        } else {
            providersPrice[ai_method_id_][msg.sender] = containerCost_;
        }
    }

    //---------------------------- Seller
    function sell(string memory name_, uint32 ram_, uint24 cpus_, bool gpu_, uint price_, string memory ipfsCID_, bool onlyAllowedUsers_, string memory dockerHubLink_) public payable returns(uint32) {
        require(msg.value >= feeMake, "e1"); // payment too small
        methodCounter += 1;
        aiMethods[methodCounter] = AiMethod(msg.sender, name_, ram_, cpus_, gpu_, price_, false, true, ipfsCID_, onlyAllowedUsers_, methodCounter, false, dockerHubLink_);
        eth[feeAccount] += msg.value;
        return methodCounter;
    }

    function updateAiMethod(uint32 ai_method_id_, string memory name_, uint32 ram_, uint24 cpus_, bool gpu_, uint price_, bool active_, string memory ipfsCID_, bool onlyAllowedUsers_, bool deleted_, string memory dockerHubLink_) public {
        AiMethod storage method = aiMethods[ai_method_id_];
        require(method.ram != 0, "e1"); // this AI method does not exist
        require(method.creator == msg.sender || admin == msg.sender, "e2"); // only method creator or admin can update AI method
        method.name = name_;
        method.ram = ram_;
        method.cpu = cpus_;
        method.gpuRequired = gpu_;
        method.price = price_;
        method.Active = active_;
        method.ipfsCID = ipfsCID_;
        method.onlyAllowedUsers = onlyAllowedUsers_;
        method.deleted = deleted_;
        method.dockerHubLink = dockerHubLink_;
    }
    
    //---------------------------- Buyer
    function buy(uint32 ai_method_id_, address provider_, uint container_price_, uint ai_method_price_, uint24 timeInSec_, string memory videoStreamUrl_) public payable returns(uint32) {
        AiMethod memory method = aiMethods[ai_method_id_];
        CProvider storage provider = hosts[provider_];
        require(provider.Allowed && method.Allowed && method.Active, "e1"); // provider and method must be allowed and active
        require(providersPrice[ai_method_id_][provider_] == container_price_ && ai_method_price_ == method.price && container_price_ != 0, "e2"); // maybe the price has changed
        require(timeInSec_ >= minTime && timeInSec_ <= maxTime, "e3"); // timeInSec must be between minTime and maxTime
        uint tmp = safeMul(timeInSec_, safeAdd(container_price_, ai_method_price_));
        tmp = tmp + (safeMul(tmp, feeTake) / 1000);
        require(tmp <= msg.value, "e4"); // not enough eth
        dealIdCounter += 1;
        emit AssignedDealId(dealIdCounter);
        if (method.onlyAllowedUsers) {
            // call oracle - handleBuyEvent is called in callback
            bytes32 requestId = isUserAllowedToUseMethod(msg.sender, ai_method_id_);
            buyAiData[requestId] = BuyAiData(ai_method_id_, provider_, container_price_, ai_method_price_, timeInSec_, videoStreamUrl_, dealIdCounter, block.number, msg.value, msg.sender);
        } else {
            // emit event immediately
            handleBuyEvent(ai_method_id_, provider_, container_price_, ai_method_price_, timeInSec_, videoStreamUrl_, block.number, msg.value, msg.sender);
        }
        return dealIdCounter;
    }
    
    function handleBuyEvent(uint32 ai_method_id_, address provider_, uint container_price_, uint ai_method_price_, uint24 timeInSec_, string memory videoStreamUrl_, uint256 blockNumber_, uint256 msgValue_, address msgSender_) internal {
        AiMethod memory method = aiMethods[ai_method_id_];
        CProvider storage provider = hosts[provider_];
        uint ethToAiMethodCreator = safeMul(ai_method_price_, timeInSec_);
        uint ethToProvider = safeMul(container_price_, timeInSec_);
        uint32 waitUntil = uint32(blockNumber_ + waitNumberOfBlocksBeforeStart);
        
        locked[dealIdCounter] = LockedFunds(ethToProvider, provider_, ethToAiMethodCreator, method.creator, safeSub(safeSub(msgValue_, ethToAiMethodCreator), ethToProvider), waitUntil, msgSender_, timeInSec_, ai_method_id_, "");
        // RAM, CPU, instances and GPU reservation 
        provider.usedRam = uint32(safeAdd(provider.usedRam, method.ram));
        provider.usedCpus = uint24(safeAdd(provider.usedCpus, method.cpu));
        provider.usedInstances = uint24(safeAdd(provider.usedInstances, 1));
        if (method.gpuRequired) {
            provider.usedGpus = uint24(safeAdd(provider.usedGpus, 1));
        }
        require(provider.usedRam <= provider.maxRam && provider.usedCpus <= provider.maxCpus && provider.usedInstances <= provider.maxRunning && provider.usedGpus <= provider.maxGpus, "e6"); // current provider doesn't have enough (ram or cpus gpu or instances)
        emit BuyAi(msgSender_, ai_method_id_, provider_, timeInSec_, dealIdCounter, videoStreamUrl_);
    }

    /*
    function complaint(uint32 dealId_, string memory text_) public {
        LockedFunds storage funds = locked[dealId_];
        require(funds.releaseBlock != 0, "e1"); // dealId not found
        require(funds.buyer == msg.sender, "e2"); // only buyer can write complaint
        require(bytes(funds.complaintText).length > 0, "c3"); // complaint already written
        funds.complaintText = text_;
    }
    */

    function releaseFunds(uint32 dealId_) private {
        LockedFunds storage funds = locked[dealId_];
        require(funds.buyer != address(0), "e1"); // deal doesn't exist
        uint256 ethProvider = funds.ethProvider;
        uint256 ethAiCreator = funds.ethAiCreator;
        uint256 ethFee = funds.ethFee;
        funds.ethProvider = 0;
        funds.ethAiCreator = 0;
        funds.ethFee = 0;
        eth[funds.provider] += ethProvider;
        eth[funds.aiCreator] += ethAiCreator;
        eth[feeAccount] += ethFee;
    }
    
    function returnToBuyer(uint32 dealId_) private onlyAdmin {
        LockedFunds storage funds = locked[dealId_];
        require(funds.buyer != address(0), "e1"); // deal doesn't exist
        uint ethProvider = funds.ethProvider;
        uint ethAiCreator = funds.ethAiCreator;
        uint ethFee = funds.ethFee;
        funds.ethProvider = 0;
        funds.ethAiCreator = 0;
        funds.ethFee = 0;
        eth[funds.buyer] += ethProvider;
        eth[funds.buyer] += ethAiCreator;
        eth[funds.buyer] += ethFee;
    }

    function getPricesOfProviders(uint32 ai_method_id_, address[] calldata providers_) public view returns(uint[] memory prices) {
        prices = new uint[](providers_.length);
        for (uint32 i = 0; i < providers_.length; i++) {
            prices[i] = providersPrice[ai_method_id_][providers_[i]];
        }
        return prices;
    }

    function getMethods(uint32 from_, uint32 to_) public view returns(AiMethod[] memory methods) {
        methods = new AiMethod[](safeSub(to_, from_) + 1);
        uint32 j = 0;
        for (uint32 i = from_; i <= to_; i++) {
            if (!aiMethods[i].deleted) {
                methods[j] = aiMethods[i];
                j++;
            }
        }
        return methods;
    }

    function getProviders(uint32 from_, uint32 to_) public view returns(CProvider[] memory providers) {
        providers = new CProvider[](safeSub(to_, from_) + 1);
        uint32 j = 0;
        for (uint32 i = from_; i <= to_; i++) {
            if (hostsIndex[i] != address(0)) {
                providers[j] = hosts[hostsIndex[i]];
                j++;
            }
        }
        return providers;
    }

    function withdraw() public {
        uint amount = eth[msg.sender];
        eth[msg.sender] = 0;
        msg.sender.transfer(amount);
    }

    /* Oracle */
    function isUserAllowedToUseMethod(address user_, uint32 ai_method_id_) private returns(bytes32 requestId) {
        Chainlink.Request memory request = buildChainlinkRequest(jobId, address(this), this.fulfill.selector);
        string memory url = strConcat(url0, addressToString(user_), url1, uint2str(ai_method_id_), url2);
        request.add("get", url);
        request.add("path", "isAllowed");
        return sendChainlinkRequestTo(oracleAddr, request, oracleFee);
    }

    /* called by oracle (callback) */
    function fulfill(bytes32 _requestId, uint256 _allowInt) public recordChainlinkFulfillment(_requestId) {
        fulfillCounter += 1;
        BuyAiData memory data = buyAiData[_requestId];
        bool allow = _allowInt == 1;
        emit UserAllowedOracle(data.dealId, allow);
        // don't use require here, if it fails, fullfil will *NEVER* be called (transaction rollback)
        //require(allow && data.provider != address(0), "e1");
        if (allow && data.provider != address(0)) {
            handleBuyEvent(data.aiMethodId, data.provider, data.containerPrice, data.aiMethodPrice, data.timeInSec, data.videoStreamUrl, data.blockNumber, data.msgValue, data.msgSender);
        }
    }
}