pragma solidity ^0.4.25;

import "./FixedPrice.sol";

contract TimeLimitedUsage is FixedPrice {

    bool serviceInUsage = false;
    uint remainingFunds;
    mapping(address => AccountData) accounts;

    event StartVCServiceEvent(bool _successPayment, uint _remainingFunds, uint _currentTime, uint _releaseTime);
    event StopVCServiceEvent(uint _remainingFunds, uint _duration);

    struct AccountData {
        uint balance;
        uint lockTimeS;
        uint releaseTime;
    }

    constructor(uint _price, uint _durationSeconds, address _endUserAddress) FixedPrice(_price, _durationSeconds, _endUserAddress) public {
        require(price > 0);
        remainingFunds = _price;
        accounts[_endUserAddress].balance = 0;
    }

    // pay the max amount and lock funds
    function payVCService() public payable {
        require(msg.sender == endUserAddress);
        assert(serviceInUsage == false);
        assert(accounts[msg.sender].balance == 0);
        require(msg.value >= price);
        remainingFunds = msg.value;
    }

    function startVCService() public {
        require(remainingFunds > 0);
        require(serviceInUsage == false);
        serviceInUsage = true;

        // history of accesses can be deducted from blockchain function calls
        accounts[msg.sender].lockTimeS = now;
        accounts[msg.sender].balance = msg.value;
        uint currentTime = now;
        accounts[msg.sender].releaseTime = currentTime + durationSeconds;

        emit StartVCServiceEvent(true, remainingFunds, currentTime, accounts[msg.sender].releaseTime);
    }


    function stopVCService() public {
        // the end user has the possibility to stop the service and
        // also VC owner can stop the service if funds are depleted
        require(msg.sender == endUserAddress || msg.sender == owner);
        require(remainingFunds > 0);
        uint currentTime = now;

        uint durationS = currentTime - accounts[msg.sender].lockTimeS;
        remainingFunds -= price / 3600 * durationSeconds;

        if (remainingFunds <= 0)
            serviceInUsage = true;
        else
            serviceInUsage = false;

        emit StopVCServiceEvent(remainingFunds, durationS);
    }

    function getAvailableFunds() returns (uint x) {
        return remainingFunds;
    }
}